/**
 * Policy Mappings
 * (sails.config.policies)
 *
 * Policies are simple functions which run **before** your actions.
 *
 * For more information on configuring policies, check out:
 * https://sailsjs.com/docs/concepts/policies
 */

module.exports.policies = {

  /***************************************************************************
  *                                                                          *
  * Default policy for all controllers and actions, unless overridden.       *
  * (`true` allows public access)                                            *
  *                                                                          *
  ***************************************************************************/

  // '*': true,

  // Allow model REST API for admin
  '*': ['isAuthenticated'],
 
  AuthController: {
    'changePassword': 'isAuthenticated',
    '*': true
  },

  /*
  ConfigController: {
    '*': 'isAuthenticated',
    'config': true,
  },
  */

  /*
  FileController: {
    'create': 'isAuthenticated',
    'find': 'isAuthenticated',
    'createInBase64': 'isAuthenticated',
  },
  */

  /*
  ProfileController: {
    'getProfile': 'isAuthenticated',
    'updateProfile': 'isAuthenticated'
  },
  */
  
  /*
  QuestionnaireController: {
    'create': 'isAuthenticated',
    'getQuestionnaireByUidTypeName': 'isAuthenticated'
  },
  */

  /*
  UserController: {
    'addDeviceToken': 'isAuthenticated'
  },
  */

  NotificationController: {
    'create': true,
    'getNotificationByUid': 'isAuthenticated',
    'getNotificationById': 'isAuthenticated',
    'updateNotificationReadById': 'isAuthenticated',    
  },

  /*
  EventController: {
    'create': 'isAuthenticated',
  },
  */

  /*
  StudyController: {
    'updateState': 'isAuthenticated',
  },
  */

  /*
  ContactController: {
    'sendContactEmail': 'isAuthenticated',
  },
  */

  /*
  ActionPlanController: {
    'create': 'isAuthenticated',
    'getActionPlanByUid': 'isAuthenticated',
  },
  */


};
