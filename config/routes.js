/**
 * Route Mappings
 * (sails.config.routes)
 *
 * Your routes tell Sails what to do each time it receives a request.
 *
 * For more information on configuring custom routes, check out:
 * https://sailsjs.com/anatomy/config/routes-js
 */

/*

If frontend cannot connect to endpoint, make sure cordova-plugin-ionic-webview is latest version.

*/

module.exports.routes = {

    // Authentication Controller 
    'POST   /api/v1/auth/signup': {
        action: 'auth/signup',
    },
    'POST   /api/v1/auth/signin': {
        action: 'auth/signin',
    },
    'POST   /api/v1/auth/refresh': {
        action: 'auth/refresh',
    },
    'PATCH  /api/v1/auth/password': {
        action: 'auth/changePassword',
    },
    'POST  /api/v1/auth/password': {
        action: 'auth/resetPassword',
    },
    'POST  /api/v1/auth/forgot': {
        action: 'auth/forgotEmail',
    },

    // File Controller
    /*
    'GET    /api/v1/file/:uuid': {
        action: 'file/find',
    },
    'POST   /api/v1/file': {
        action: 'file/create',
    },
    'POST   /api/v1/file/base64': {
        action: 'file/createInBase64',
    },
    */

    // Profile Controller
    'GET   /api/v1/profile': {
        action: 'profile/getProfile',
    },
    'PATCH   /api/v1/profile': {
        action: 'profile/updateProfile',
    },

    // User Controller
    'PATCH   /api/v1/user/deviceToken/:device_token': {
        action: 'user/addDeviceToken',
    },

    // Questionnarie Controller
    'POST   /api/v1/questionnaire': {
        action: 'questionnaire/create',
    },
    'GET   /api/v1/questionnaire': {
        action: 'questionnaire/getQuestionnaireByUidTypeName',
    },

    // Notification Controller
    'POST   /api/v1/notification': {
        action: 'notification/create',
    },
    'GET   /api/v1/notification': {
        action: 'notification/getNotificationByUid',
    },
    'GET   /api/v1/notification/:id': {
        action: 'notification/getNotificationById',
    },
    'PATCH   /api/v1/notification/read/:id': {
        action: 'notification/updateNotificationReadById',
    },

    // Config Controller
    /*
    'GET   /api/v1/config': {
        action: 'config/config',
    },
    */

    // Event Controller
    'POST   /api/v1/event': {
        action: 'event/create',
    },

    // Study Controller
    /*
    'PATCH   /api/v1/study/state': {
        action: 'study/updateState',
    },
    */

    // Contact Controller
    'POST /api/v1/contact': {
        action: 'contact/sendContactEmail',
    },

    // Action Plan Controller
    'POST /api/v1/actionplan': {
        action: 'actionplan/create',
    },
    'GET  /api/v1/actionplan': {
        action: 'actionplan/getActionPlanByUid',
    },


};
