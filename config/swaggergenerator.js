module.exports["swagger-generator"] = {
    disabled: true,
    swaggerJsonPath: "../swagger.json",     // Use external path to avoid nodemon keep restarting
    parameters: { //we can add up custom parameters here
        PerPageQueryParam: {
            in: 'query',
            name: 'perPage',
            required: false,
            type: 'integer',
            description: 'This helps with pagination and when the limit is known for pagify'
        }
    },
    blueprint_parameters: { list: [{ $ref: '#/parameters/PerPageQueryParam' }] },//we can add custom blueprint action to parameters binding here, any specified overrides default created
    swagger: {
        swagger: '2.0',
        info: {
            title: 'Swagger Json',
            description: 'This is a generated swagger json for Gambling API',
            termsOfService: '',
            contact: { name: 'Andy Leung', email: 'andy.leung@auckland.ac.nz', url: 'https://www.nihi.auckland.ac.nz/'},
            license: {name: 'NIHI', url: 'https://www.nihi.auckland.ac.nz/'},
            version: '1.0.0'
        },
        host: 'localhost:3000',
        basePath: '/',
        externalDocs: { url: 'https://www.nihi.auckland.ac.nz/' }
    }
};