/**
 * Passport configuration file where you should configure strategies
 */

// Check which environment
var env = null;
switch(process.env.NODE_ENV) {
    case 'stage':
        env = require('./env/stage');
        break;
    case 'production':
        env = require('./env/production');
        break;    
    default:
        env = require('./custom');
        break;
}

var passport = require('passport');
var LocalStrategy = require('passport-local').Strategy;
var JwtStrategy = require('passport-jwt').Strategy;
var ExtractJwt = require('passport-jwt').ExtractJwt;

// Expires time in access token
var EXPIRES_IN = "1 day";
// Expires time of refresh token
var REFRESH_EXPIRES_IN = "14 days";
// Access token secret
var SECRET = env.custom.tokenSecret;
// Refersh token secret
var REFRESH_SECRET = env.custom.refreshTokenSecret;
var ALGORITHM = "HS256";
var ISSUER = "nihi.auckland.ac.nz";
var AUDIENCE = "mpr";

/**
 * Configuration object for local strategy
 */
var LOCAL_STRATEGY_CONFIG = {
    usernameField: 'study_id',
    passwordField: 'password',
    passReqToCallback: false
};

/**
 * Configuration object for JWT strategy
 */
var JWT_STRATEGY_CONFIG = {
    secretOrKey: SECRET,
    issuer: ISSUER,
    audience: AUDIENCE,
    passReqToCallback: false,
    jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken()
};

/**
 * Configuration object for JWT strategy
 */
var REFRESH_JWT_STRATEGY_CONFIG = {
    secretOrKey: REFRESH_SECRET,
    issuer: ISSUER,
    audience: AUDIENCE,
    passReqToCallback: false,
    jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken()
};

/**
 * Triggers when user authenticates via local strategy
 * It accepts email or username for sign in. If email format, then found by email else by username
 */
/*
function _onLocalStrategyAuth(email, password, next) {
    if (_isEmail(email)) {
        UserService.findByEmail(email).then(user => {
                var errMsg = _checkUser(user, password);
                if (errMsg) {
                    return next(null, false, errMsg);
                } else {
                    EventService.createEvent(user.id, {name:'Sign in by email'});
                    return next(null, user, {});
                }
            }).catch(err => {
                return next(err, false, {});
            });
    } else {
        UserService.findByUsername(email).then(user => {
            var errMsg = _checkUser(user, password);
            if (errMsg) {
                return next(null, false, errMsg);
            } else {
                EventService.createEvent(user.id, {name:'Sign in by username'});
                return next(null, user, {});
            }
        }).catch(err => {
            return next(err, false, {});
        });
    }
}
*/

function _onLocalStrategyAuth(study_id, password, next) {

    UserService.findByStudyID(study_id).then(
        user => {
            let errMsg = _checkUser(user, password);
            if (errMsg) {
                return next(null, false, errMsg);
            }
            else {
                EventService.createEvent(user.id, { name: 'Signed in by Study ID' });
                return next(null, user, {});
            }
        }        
    ).catch(
        err => {
            return next(err, false, {});
        }
    );

}




/**
 * Check user is active or not
 * @param {Object} user 
 * @param {String} password 
 */
function _checkUser(user, password) {

    if (!user) {
        return {
            error: {
                code: 'E_USER_NOT_FOUND',
                message: 'User not found',
            },            
        };
    } 
    else {
        // TODO: replace with new cipher service type
        if (!CipherService.comparePassword(password, user)) {
            return {
                error: {
                    code: 'E_WRONG_PASSWORD',
                    message: 'Password is wrong',
                },                
            };
        }

        if (user.status !== 1) {
            return {
                error: {
                    code: 'E_DEACTIVATED',
                    message: 'Account is deactivated',
                },
            };
        }

        return null;

    }
}

/**
 * Test the input is email format (simpily check)
 * @param {String} email 
 */
function _isEmail(email) {
    var re = /^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/;
    return re.test(email);
}

/**
 * Triggers when user authenticates via JWT strategy
 */
function _onJwtStrategyAuth(payload, next) {
    // User may be old value that store in payload during generate token
    var user = payload.user;
    return next(null, user, {});
}

/**
 * Triggers when user authenticates via JWT strategy
 */
function _onJwtRefreshStrategyAuth(payload, next) {
    // Get latest data
    User.findOne({ 'study_id': payload.user.study_id })
        .exec(function (error, user) {
            // Check if Token generated before revoke time, reject refresh
            if (user.revoke_time > payload.iat) {
                return next(null, false, {
                    code: 'E_TOKEN_REVOKED',
                    message: 'Token is revoked'
                });
            }
            if (user.status != 1) {
                return next(null, false, {
                    code: 'E_DEACTIVATED',
                    message: 'Account is deactivated'
                });
            }
            return next(null, user, {});
        });
}

passport.use('local',
    new LocalStrategy(LOCAL_STRATEGY_CONFIG, _onLocalStrategyAuth));
passport.use('jwt',
    new JwtStrategy(JWT_STRATEGY_CONFIG, _onJwtStrategyAuth));
passport.use('refresh',
    new JwtStrategy(REFRESH_JWT_STRATEGY_CONFIG, _onJwtRefreshStrategyAuth));

module.exports.jwtSettings = {
    expiresIn: EXPIRES_IN,
    secret: SECRET,
    algorithm: ALGORITHM,
    issuer: ISSUER,
    audience: AUDIENCE
};

module.exports.refreshJwtSettings = {
    expiresIn: REFRESH_EXPIRES_IN,
    secret: REFRESH_SECRET,
    algorithm: ALGORITHM,
    issuer: ISSUER,
    audience: AUDIENCE
};

