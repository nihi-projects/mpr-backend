/**
 * Sails Seed Settings
 * (sails.config.seeds)
 *
 * Configuration for the data seeding in Sails.
 *
 * For more information on configuration, check out:
 * http://github.com/frostme/sails-seed
 */

// Made obsolete by bootstrap.js
module.exports.seeds = {

    /*
    participant: [

        { study_id: 986269 }, { study_id: 545271 }, { study_id: 782090 }, { study_id: 698942 }, { study_id: 293332 }, { study_id: 199117 }, { study_id: 523903 }, { study_id: 972341 }, { study_id: 251372 }, { study_id: 378629 }, { study_id: 318135 }, { study_id: 660368 }, { study_id: 319449 }, { study_id: 292479 }, { study_id: 974722 }, { study_id: 443741 }, { study_id: 336900 }, { study_id: 251389 }, { study_id: 297225 }, { study_id: 635009 }, { study_id: 899162 }, { study_id: 614643 }, { study_id: 599161 }, { study_id: 562669 }, { study_id: 895431 }, { study_id: 806054 }, { study_id: 613292 }, { study_id: 204990 }, { study_id: 449892 }, { study_id: 272078 }, { study_id: 826285 }, { study_id: 291515 }, { study_id: 628394 }, { study_id: 962283 }, { study_id: 463454 }, { study_id: 101853 }, { study_id: 106859 }, { study_id: 145106 }, { study_id: 940962 }, { study_id: 608692 }, { study_id: 689666 }, { study_id: 981210 }, { study_id: 519364 }, { study_id: 216504 }, { study_id: 458769 }, 

    ]
    */

}

