module.exports = {
    apps: [{
        // name should same as Jenkins project folder
        name: 'mpr-api',
        script: '/var/sails/mpr-api/app.js',

        // log files
        log_file: '/var/log/sails/mpr-api.log',
        out_file: '/var/log/sails/mpr-api_out.log',
        error_file: '/var/log/sails/mpr-api_err.log',

        watch: false,

        // different environments
        env: {
            NODE_ENV: 'local'
        },
        env_stage: {
            NODE_ENV: 'stage'
        },
        env_production: {
            NODE_ENV: 'production'
        }
    }]
};
