/**
 * ConfigController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */

module.exports = {
  
    /**
     * get json config file
     *
     * (GET /config)
     */
    config: function (req, res) {
        var file = './app/config.json';
        const jsonfile = require('jsonfile');

        jsonfile.readFile(file, function (err, obj) {
            if (err) {
              res.json({err: err});
            }
            res.json(obj);
        })
    },

};

