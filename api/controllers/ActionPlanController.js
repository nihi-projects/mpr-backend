module.exports = {

    create: function(req, res) {

        ActionPlanService.createActionPlan(req.user.id, req.allParams()).then(
            (new_action_plan) => {
                return res.created(new_action_plan);
            }            
        ).catch(
            (err) => {
                return res.serverError(err);
            }
        );
    },


    getActionPlanByUid: async function(req, res) {

        res.json(await ActionPlanService.findByUid(req.user.id));

    },


};