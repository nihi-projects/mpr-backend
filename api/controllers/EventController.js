/**
 * EventController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */

module.exports = {
  
    /**
     * create new event
     *
     * (POST /event)
     */
    create: function (req, res) {
        var event = _.omit(req.allParams(), ['id']);
        
        EventService.createEvent(req.user.id, event)
            .then(newEvent => {
                return res.created(newEvent);
            })
            .catch(err => {
                return res.serverError(err);
            });
        
    },
};

