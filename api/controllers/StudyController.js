/**
 * StudyController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */

module.exports = {
    /**
     * Update state of currently logged-in user
     *
     * (PATCH /state)
     */
    updateState: function(req, res) {
        var study = req.allParams().study;
        
        if (study && study.state) {
            var update = { "state": study.state };
            UserService.updateStudyById(req.user.id, update).then(newStudy => {
                return res.json(newStudy);
            }).catch(err => {
                return res.serverError(err);
            });
        } else {
            return res.serverError("study.state is not found in request");
        }
    },
}