/**
 * FileController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */

module.exports = {

    /**
     * upload file in binary format
     *
     * (POST /file)
     */
    create: function (req, res) {
        FileService.createFile(req.user.id, req.file('file'))
            .then(file => {
                return res.created(file);
            })
            .catch(err => {
                return res.serverError(err);
            });
    },


    /**
     * Download file with uuid
     *
     * (GET /file/:uuid)
     */
    find: function (req, res) {
        File.findOne({ uuid: req.param('uuid') })
            .exec(function (error, file) {
                if (error) return next(error, false, {});

                if (!file) return res.notFound();

                // set the filename to the same file as the user uploaded
                res.set("Content-disposition", "attachment; filename='" + file.name + "'");

                FileService.getFile(file.fd).on('error', function (err) {
                    return res.serverError(err);
                })
                    .pipe(res);
            });
    },

    /**
     * Upload file in base64
     *
     * (POST /file)
     */
    createInBase64: function (req, res) {
        
        var data = req.allParams().file;
        var filename = req.allParams().filename;

        FileService.createFileInBase64(req.user.id, filename, data)
            .then(file => {
                return res.created(file);
            })
            .catch(err => {
                return res.serverError(err);
            });
        

    },
};

