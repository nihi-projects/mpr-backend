/**
 * NotificationController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */

module.exports = {
  
    /**
     * create new notification
     *
     * (POST /notification)
     */
    create: function (req, res) {
        
        NotificationService.sendNotification(req.param('uid')).then( notification => {
            return res.created(notification);
        }).catch(err => {
            return res.serverError(err);
        });
        
    },

    /**
     * Get notification of currently logged-in user
     *
     * (GET /notification)
     */
    getNotificationByUid: async function(req, res) {

        NotificationService.findByUid(req.user.id).then(notifications => {
            return res.json(notifications);
        }).catch(err => {
            return res.serverError(err);
        });
    },

    /**
     * Get notification by id
     *
     * (GET /notification/:id)
     */
    getNotificationById: async function(req, res) {
        var id = req.param('id');
        NotificationService.findByUidId(req.user.id, id).then(notification => {
            if (!notification) return res.notFound();

            return res.json(notification);
        }).catch(err => {
            return res.serverError(err);
        });
    },

    /**
     * Update notification read / unread by id
     *
     * (PATCH /notification/read/:id)
     */
    updateNotificationReadById: function(req, res) {
        var id = req.param('id');
        var read = req.param('read');

        if ((typeof read)!="boolean") {
            return res.badRequest("Missing / Invalid value");
        }
        updateNotification = { "read": read}

        NotificationService.updateNotificationById(id, updateNotification).then(notification => {
            if (!notification) return res.notFound();

            return res.json(notification);
        }).catch(err => {
            return res.serverError(err);
        });
    },
};

