/**
 * UserController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */

module.exports = {
  
    // Change device
    // Revoke token
    // Deactive account?

    // Add device token
    addDeviceToken: function(req, res) {
        var newDeviceToken = req.param('device_token');
        UserService.findById(req.user.id).then(user => {
            if (user) {
                if (!user.device_token) {
                    user.device_token = [newDeviceToken];
                    
                } else {
                    if (user.device_token.includes(newDeviceToken)) {
                        // No need update
                        return res.json(user);
                    } else {
                        user.device_token.push(newDeviceToken);
                    }
                }
                updatedToken = { "device_token" : user.device_token };
                UserService.updateUser(user.id, updatedToken).then(profile => {
                    if (profile) {
                        return res.json(profile);
                    }
                    return res.notFound();
                }).catch(err => {
                    return res.serverError(err);
                });
            } else {
                return res.notFound();
            }
        }).catch(err => {
            return res.serverError(err);
        });
    },


};

