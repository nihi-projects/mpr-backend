/**
 * ProfileController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */

module.exports = {

    /**
     * Get profile of currently logged-in user
     *
     * (GET /profile)
     */
    getProfile: function(req, res) {
        UserService.findById(req.user.id).then(profile => {
            if (profile) {
                return res.json(profile);
            }
            return res.notFound();
        }).catch(err => {
            return res.serverError(err);
        });
    },

    /**
     * Update profile of currently logged-in user
     *
     * (PATCH /profile)
     */
    updateProfile: function(req, res) {
        var profile = req.allParams().profile;
        
        if (profile) {
            UserService.updateProfileById(req.user.id, profile).then(newProfile => {
                return res.json(newProfile);
            }).catch(err => {
                return res.serverError(err);
            });
        } else {
            return res.serverError("Profile data is not found in request");
        }
    },
};

