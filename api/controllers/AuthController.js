/**
 * AuthController
 *
 * @description :: Server-side logic for manage user's authorization
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */

var passport = require('passport');
/**
 * Triggers when user authenticates via passport
 * @param {Object} req Request object
 * @param {Object} res Response object
 * @param {Object} error Error object
 * @param {Object} user User profile
 * @param {Object} info Info if some error occurs
 * @private
 */
function _onPassportAuth(req, res, error, user, info) {
    if (error) return res.serverError({ error: error });
    if (!user) return res.unauthorized(null, info && info.code, info && info.message);

    return res.ok({
        // TODO: replace with new type of cipher service
        token: CipherService.createToken(user),
        refreshToken: CipherService.createRefeshToken(user),
        user: user
    });
}

module.exports = {
    /**
     * Sign up in system
     * @param {Object} req Request object
     * @param {Object} res Response object
     */
    signup: async function (req, res) {
        var user = _.omit(req.allParams(), ['id']);
        user.role = User.roles.user;
        
        try{

            let participant = await Participant.find({ study_id: user.study_id });
            
            if (participant.length === 0) {
                res.serverError({
                        error: {
                            message: 'The Study ID number entered is not valid.',
                        }
                });
            }
            else {

                // await to wait for created id        
                let createdUser = await UserService.createUser(user);        
                await EventService.createEvent(createdUser.id, { name: 'Signed up' });

                res.created({
                    // TODO: replace with new type of cipher service
                    token: CipherService.createToken(createdUser),
                    refreshToken: CipherService.createRefeshToken(createdUser),
                    user: createdUser
                });

            }

        }
        catch(err) {
            res.serverError({
                error: {
                    code: err.code,
                    message: err.message,
                }
            });
        }
        
    },

    /**
     * Sign in by local strategy in passport
     * @param {Object} req Request object
     * @param {Object} res Response object
     */
    signin: function (req, res) {
        passport.authenticate('local',
            _onPassportAuth.bind(this, req, res))(req, res);
    },

    /**
     * Sign in by local strategy in passport
     * @param {Object} req Request object
     * @param {Object} res Response object
     */
    refresh: function (req, res) {
        passport.authenticate('refresh',
            _onPassportAuth.bind(this, req, res))(req, res);
    },

    /**
     * reset password
     */
    resetPassword: function(req, res) {
        let study_id = req.param('study_id');
        let email = req.param('email');

        UserService.findByStudyID(study_id).then(
            user => {

                if (!user) {
                    return res.notFound();
                }
                else {

                    if (user.profile.email === email) {

                        let newPassword = sails.helpers.generatePassword();
                        user.password = newPassword;
                        
                        UserService.updateUser(user.id, user).then(
                            user => {

                                if (user) {
                                    MailerService.forgotPasswordMail(email, user.profile.username, newPassword);
                                    EventService.createEvent(user.id, {name:'Reset password'});
                                    return res.json(user);
                                }

                                return res.notFound();
                            }
                        );

                    }
                    else {
                        return res.notFound();
                    }                    

                }

            }
        );

    },

    /**
     * change password
     */
    changePassword: function(req, res) {
        let oldPassword = req.param('old_password');
        let newPassword = req.param('new_password');
        UserService.findById(req.user.id).then(user => {
            if (!CipherService.comparePassword(oldPassword, user)) {
                return res.unauthorized("Invalid password");
            } else {
                user.password = newPassword;
                UserService.updateUser(user.id, user).then(profile => {
                    if (profile) {
                        EventService.createEvent(user.id, {name:'Change password'});
                        return res.json(profile);
                    }
                    return res.notFound();
                });
            }
        })
    },

    /**
     * forgot email
     */
    forgotEmail: function(req, res) {
        var dob = req.param('dob');
        var region = req.param('region');
        var gender = req.param('gender');
        var phoneNo = req.param('phoneNo');
        if (dob && region && gender && phoneNo) {
            MailerService.forgotEmailMail(dob, region, gender, phoneNo);
            return res.created();
        } else {
            return res.badRequest("Missing / Invalid value");
        }
    },
};
