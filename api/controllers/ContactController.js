
module.exports = {

    /**
     * Send contact email
     * @param {Object} req Request object
     * @param {Object} res Response object
     */
    sendContactEmail: async function (req, res) {

        let contactEmail = req.allParams();

        try{

            MailerService.sendContactEmail(contactEmail);

            res.ok(contactEmail);

        }
        catch(err) {
            res.ok({
                error: {
                    code: err.code,
                    message: err.message,
                }
            });
        }
        
    },
    
};
