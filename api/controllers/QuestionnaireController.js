/**
 * QuestionnaireController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */

module.exports = {
  
    /**
     * create new questionnaire 
     *
     * (POST /questionnaire)
     */
    create: function (req, res) {
        var questionnaire = _.omit(req.allParams(), ['id']);
        QuestionnaireService.createQuestionnaire(req.user.id, questionnaire).then( newQuestionnaire => {
            return res.created(newQuestionnaire);
        }).catch(err => {
            return res.serverError(err);
        });
        
    },

    /**
     * Get questionnaire of currently logged-in user
     *
     * (GET /questionnaire)
     */
    getQuestionnaireByUidTypeName: async function(req, res) {
        return res.json(await QuestionnaireService.findByUidTypeName(req.user.id, req.param('type'), req.param('name')));
    },
};

