var bcrypt = require('bcrypt-nodejs');
var jwt = require('jsonwebtoken');

module.exports = {
  secret: sails.config.jwtSettings.secret,
  issuer: sails.config.jwtSettings.issuer,
  audience: sails.config.jwtSettings.audience,

  /**
   * Hash the password field of the passed user.
   */
  hashPassword: function (user) {
    if (user.password) {
      user.password = bcrypt.hashSync(user.password);
    }
  },

  /**
   * Compare user password hash with unhashed password
   * @returns boolean indicating a match
   */
  comparePassword: function (password, user) {
    return bcrypt.compareSync(password, user.password);
  },

  /**
   * Create a token based on the passed user
   * @param user
   */
  createToken: function (user) {
    return jwt.sign({
      user: user
    },
      sails.config.jwtSettings.secret,
      {
        algorithm: sails.config.jwtSettings.algorithm,
        expiresIn: sails.config.jwtSettings.expiresIn,
        issuer: sails.config.jwtSettings.issuer,
        audience: sails.config.jwtSettings.audience,
        noTimestamp: false
      }
    );
  },

  /**
   * Create a refresh token based on the passed user
   * @param user
   */
  createRefeshToken: function (user) {
    return jwt.sign({
      user: user
    },
      sails.config.refreshJwtSettings.secret,
      {
        algorithm: sails.config.refreshJwtSettings.algorithm,
        expiresIn: sails.config.refreshJwtSettings.expiresIn,
        issuer: sails.config.refreshJwtSettings.issuer,
        audience: sails.config.refreshJwtSettings.audience,
        noTimestamp: false
      }
    );
  }
};