module.exports = {

    /**
     * Create event
     */
    createEvent: function(uid, event) {
        event.createdBy = uid;
        event.updatedBy = uid;

        return new Promise(function(resolve, reject) {
            Event.create(event).fetch().exec(
                function(err, created) {
                    if (err) return reject(err);
                    return resolve(created);
                }
            );
        })
    },
    
}