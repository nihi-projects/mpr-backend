/*
// Message JSON structure
{
 "to" : "YOUR_FCM_TOKEN_WILL_BE_HERE",
 "collapse_key" : "type_a", 
 "data" : {
     "body" : "Body of Your Notification in Data",
     "title": "Title of Your Notification in Title",
     "key_1" : "Value for key_1",
     "key_2" : "Value for key_2"
 }
}
*/

module.exports = {

    sendToFCM: async function(message) {
        
        let request = require('request');
        let serverKey = sails.config.custom.fcmServerKey;
           
           request(
               { 
                    url: 'https://fcm.googleapis.com/fcm/send',
                    method: "POST", 
                    proxy: sails.config.custom.httpProxy,
                    headers: { 
                    "Content-Type": "application/json",
                    "Authorization": "key=" + serverKey,                   
                    }, 
                    json: message
                }, 
                function(error, response, body) {
                
                    if (error) {
                        console.log('Something went wrong.');
                        console.log(error);
                    }
                    else {
                        console.log('Response received.');
                        console.log(body);                        
                    }
                    
                }
            );

    },

    /**
     * Find notification by user id and notification id
     */
    findByUidId: function(uid, id) {
        return new Promise(function(resolve, reject) {
            Notification.findOne({ createdBy: uid, id: id }).populateAll().then( function(notification) {
                return resolve(notification);
            }).catch(err => {
                return reject(err);
            });
        });
    },
    
    /**
     * Find all notifications by user id 
     */
    findByUid: function(uid) {
        return new Promise(function(resolve, reject) {
            Notification.find({ createdBy: uid }).populateAll().then( function(notification) {
                return resolve(notification);
            }).catch(err => {
                return reject(err);
            });
        });
    },

    /**
     * Send notification out 
     */
    sendNotification: async function(uid) {

        try {

            let user = await User.findOne({ id: uid });
            let profile = await Profile.findOne({ uid: uid });

            if (!user) {
                return { error: 'No such user found.' };
            }
            else {

                let first_name = profile.username;
                //let latest_device_token = (user.device_token !== null) ? user.device_token.pop() : null;

                if (!user.device_token) {
                    return { error: 'User has no device token assigned.' };
                }
                else {

                    let notification = {};                
                    // TODO: get notification template 
                    notification.subject = "Reminder about completing sit-to-stand test and questionnaire completion";
                    notification.body = "Hi " + first_name + " just a friendly reminder that you have sit-to-stand test and questionnaire to complete. If safe to do so, please complete both of these in your mPR app.";
                    notification.link = "https://www.google.com";
                    notification.read = false;
                    notification.createdBy = uid;
                    notification.updatedBy = uid;

                    let fcm_data = {
                        "registration_ids" : user.device_token,                           
                        "notification": {
                            "title": notification.subject,
                            "body" : notification.body,                        
                        },
                    };                 
                    
                    return new Promise(function(resolve, reject) {

                        Notification.create(notification).fetch().exec(
                            function(err, createdNotification) {
            
                                if (err) return reject(err);
            
                                module.exports.sendToFCM(fcm_data);
            
                                return resolve(createdNotification);
            
                            }
                        );
                    });

                }                

            }

        }
        catch(err) {
            console.log(err);
            return { 'error': err.message };
        }

        
        
    },

    /**
     * Update notification by notification id
     */
    updateNotificationById: function(id, newNotification) {
        return new Promise(function(resolve, reject) {
            Notification.updateOne({ id: id }).set(newNotification).exec(
                function(err, updated) {
                    if (err) return reject(err);

                    if (!updated) return resolve(null);

                    return resolve(updated);
                }
            )
        });
    },
}