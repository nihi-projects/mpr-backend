/**
 * find questionnaire by criteria
 */
findQuestionnaireByCriteria = async function(criteria) {
    var questionnaires = await Questionnaire.find(criteria);

    if (questionnaires) {
        for (var i =0; i < questionnaires.length; i++) {
            // populates question and answer
            var questionnaire = questionnaires[i];
            var questions = await Question.find({ questionnaire_id: questionnaire.id }).populate('answers');
            questionnaire.questions = questions;
            questionnaires[i] = questionnaire;
        }
    }
    return questionnaires;
};

module.exports = {

    /**
     * find questionnaire by id
     */
    findById: function(id) {
        return new Promise(function(resolve, reject) {
            Questionnaire.findOne({ id: id }).populateAll().then( function(questionnaire) {
                Question.find({ questionnaire_id: id }).populate('answers').then( function(questions) {
                    questionnaire.questions = questions;
                    return resolve(questionnaire);
                }).catch(err => {
                    return reject(err);
                });; 
            }).catch(err => {
                return reject(err);
            });
        });
    },
    
    /**
     * find questionnaire by user id with type or name
     */
    findByUidTypeName: async function(uid, type, name) {
        var criteria = { createdBy: uid };

        if (type) {
            criteria.type = type;
        }

        if (name) {
            criteria.name = name;
        }
        return await findQuestionnaireByCriteria(criteria); 
    },

    /**
     * create questionnarie
     */
    createQuestionnaire: async function(uid, questionnaire) {
        var questions = questionnaire.questions;
        questionnaire = _.omit(questionnaire, ['questions']);
        questionnaire.createdBy = uid;
        questionnaire.updatedBy = uid;
        
        var newQuestionnaire = await Questionnaire.create(questionnaire).fetch();
        
        if (questions) {
            // Create quesitons
            for (var i = 0; i < questions.length; i++) {
                var answers = questions[i].answers;
                var question = _.omit(questions[i], ['id', 'answers']);
                question.questionnaire_id = newQuestionnaire.id;
                var newQuestion = await Question.create(question).fetch();
                
                if (answers) {
                    // Create answers
                    for (var j = 0; j < answers.length; j++) {
                        var answer = answers[j];
                        answer.question_id = newQuestion.id;
                        await Answer.create(answer);
                    }
                }
            }
        }

        return await module.exports.findById(newQuestionnaire.id);
    },
}