/**
 * find user by crietria
 */
findByCriteria = function(criteria) {
    
    return new Promise(function(resolve, reject) {
        User.findOne( criteria ).populateAll().exec(
            function(err, result) {
                if (err) return reject(err);
                
                if (!result) return resolve(result);

                var profile = sails.helpers.getFirstJsonElementFromArray(result.profile);
                if (profile) {
                    result.profile = profile;
                    return resolve(result);                    
                } else {
                    delete result.profile;
                    return resolve(result);
                }
            }
        );
    });
};

/**
 * get user picture
 */
getUserPicture = function(pic_id) {
    return new Promise(function(resolve, reject) {
        FileService.getFileDataById(pic_id).then(picture => {
            if (!picture) resolve(null);

            return resolve(picture);
        }).catch(err => {
            return reject(err);
        });
    });
};

/**
 * create profile 
 */
createProfileReturnUser = function(profile) {
    return new Promise(function(resolve, reject) {
        Profile.create(profile).fetch().exec(            
            function(err, createdProfile) {
                if (err) { return reject(err); };                
                
                module.exports.findById(createdProfile.uid).then(u => {                                                            
                    return resolve(u);
                }).catch(err => {                    
                    return reject(err);
                });

            }
        );        
    });
}


/**
 * create study 
 */
createStudy = function(study) {
    return new Promise(function(resolve, reject) {
        Study.create(study).fetch().exec(
            function(err, createdStudy) {
                if (err) return reject(err);

                return resolve(createdStudy);
            }
        );        
    });
}

/**
 * update profile
 */
updateProfileReturnUser = function(profileId, newProfile) {
    return new Promise(function(resolve, reject) {
        Profile.updateOne({ id: profileId }).set(newProfile).exec(
            function(err, updatedProfile) {
                if (err) return reject(err);
                
                if (!updatedProfile) return reject("Profile not found");
                
                module.exports.findById(updatedProfile.uid).then(u => {
                    return resolve(u);
                }).catch(err => {
                    return reject(err);
                })
            }
        );        
    });
}

/**
 * update study
 */
updateStudyReturnUser = function(studyId, newStudy) {
    return new Promise(function(resolve, reject) {
        Study.updateOne({ id: studyId }).set(newStudy).exec(
            function(err, updatedStudy) {
                if (err) return reject(err);
                
                if (!updatedStudy) return reject("Study not found");
                
                module.exports.findById(updatedStudy.uid).then(u => {
                    return resolve(u);
                }).catch(err => {
                    return reject(err);
                })
            }
        );        
    });
}

module.exports = {
    /**
     * find user by id
     */
    findById: function(id) {
        return findByCriteria({ id: id });
    },

    /**
     * find user by email
     */
    findByEmail: function(email) {
        return findByCriteria({ email: email });
    },

    /**
     * find user by username
     */
    findByUsername: function(username) {
        return findByCriteria({ username: username });
    },

    findByStudyID: function(study_id) {
        return findByCriteria({ study_id: study_id });
    },    


    /**
     * 
     * @param {*} user 
     */
    createUser: function (user) {
        let profile = (user.hasOwnProperty('profile')) ? user.profile : {};        
        user = _.omit(user, ['profile']);

        return new Promise(function (resolve, reject) {

                    User.create(user).fetch().exec(
                        function (err, createdUser) {
                            
                            if (err) {                            
                                return reject(err); 
                            }
    
                            profile.createdBy = createdUser.id;
                            profile.updatedBy = createdUser.id;
                            profile.uid = createdUser.id;
    
                            createProfileReturnUser(profile).then(u => {                                                                                
                                return resolve(u);
                            }).catch(async err => { 
                                await User.destroy({ id: createdUser.id });
                                return reject(err);
                            });
    
                            
                        }
                    );

        });
    },
   

    /**
     * update user
     */
    updateUser: function(id, newUser) {
        newUser = _.omit(newUser, ['profile', 'study']);
        return new Promise(function(resolve, reject) {
            User.updateOne({ id: id }).set(newUser).exec(
                function(err, updatedUser) {
                    if (err) return reject(err);

                    if (!updatedUser) return reject("User not found");

                    module.exports.findById(id).then(u => {
                        return resolve(u);
                    }).catch(err => {
                        return reject(err);
                    })
                }
            )
        });
    },

    /**
     * update profile by id
     */
    updateProfileById: function(id, newProfile) {
        return new Promise(function(resolve, reject) {
            findByCriteria({ id: id }).then( user => {
                if (user && user.profile) {
                    
                    updateProfileReturnUser(user.profile.id, newProfile).then(u => {
                        return resolve(u);
                    }).catch(err => {
                        return reject(err);
                    })
                } else {
                    return reject("User not found");
                }
            }).catch(err => {
                return reject(err);
            })
        });
    },

    /**
     * update study by id
     */
    updateStudyById: function(id, newStudy) {
        return new Promise(function(resolve, reject) {
            findByCriteria({ id: id }).then( user => {
                if (user && user.study) {
                    updateStudyReturnUser(user.study.id, newStudy).then(u => {
                        return resolve(u);
                    }).catch(err => {
                        return reject(err);
                    })
                } else {
                    return reject("User not found");
                }
            }).catch(err => {
                return reject(err);
            })
        });
    }
}