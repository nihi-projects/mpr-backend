
module.exports = {

    findById: function(id) {
        return new Promise(function(resolve, reject) {
                ActionPlan.findOne({ id: id }).populateAll().then(
                    (action_plan) => {
                        return resolve(action_plan);
                    }
                ).catch(
                    (err) => {
                        return reject(err);
                    }
                ); 
            }
        ).catch(
                (err) => {
                    return reject(err);
                }
        );        
    },

    findByUid: async function(uid) {
        let criteria = { createdBy: uid };
        
        return await ActionPlan.find(criteria);
    },

    createActionPlan: async function (uid, action_plan) {

        action_plan.createdBy = uid;
        action_plan.updatedBy = uid;

        try {
            let newActionPlan = await ActionPlan.create(action_plan).fetch();
            return await module.exports.findById(newActionPlan.id);        
        }
        catch(err) {
            console.log(err);
            return { error: err };
        }        

    },



}