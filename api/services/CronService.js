var CronJob = require('cron').CronJob;

/*
new CronJob('* * * * * *', function() {
    console.log("test for every second");
}, null, true);
*/

module.exports = {

    testJob : function(time) {

        const job = new CronJob(
            time, 
            function() {
                console.log("trigger by function");
            }
        );

        job.start();

    },

    notificationJob: function(time) {

        const job = new CronJob(
            time,
            async function() {

                    const daysToCheck = [21, 35, 49, 63];

                    let today = new Date();

                    let users = await User.find();

                    users.forEach(

                        user => {

                            let createDate = new Date(user.createdAt);

                            let daysDifference = Math.round( (today - createDate) / 1000 / 60 / 60 / 24 );

                            if (daysToCheck.includes(daysDifference)) {
                                
                                NotificationService.sendNotification(user.id).then( notification => {
                                    console.log(notification);
                                }).catch(err => {
                                    console.log(err);
                                }); 
                                
                            }

                        }

                    );

            }
        );

        job.start();

        let today = new Date();

        console.log('Started cronjob at ' + today.toDateString());

    },


}
