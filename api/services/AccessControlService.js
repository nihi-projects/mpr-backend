
module.exports = {
    /**
     * Check is admin user.
     */
    isAdmin: function (user) {
        if (user.role === User.roles.admin) {
            return true;
        }
        return false;
    },

    /**
     * Check is admin user.
     */
    isUser: function (user) {
        if (user.role === User.roles.user) {
            return true;
        }
        return false;
    },

    /**
     * Check is own data.
     */
    accessOwnData: function (user, createdBy) {
        if (user.username ===createdBy) {
            return true;
        }
        return false;
    },
}