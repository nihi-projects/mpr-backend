const nodemailer = require("nodemailer");
var ejs = require("ejs");

// create reusable transporter object using the default SMTP transport
let transporter = nodemailer.createTransport({
    host: sails.config.custom.smtpHost,
    port: sails.config.custom.smtpPort,
    secure: false, // true for 465, false for other ports
    
  });

/**
 * Send email
 */
sendEmail = async function(email, mail) {
    
    let info = await transporter.sendMail({
        from: sails.config.custom.studyEmail,
        to: email,
        subject: mail.subject,
        html: mail.body,
    });
    
    return info;
}

module.exports = {

    forgotPasswordMail: function(email, firstname, newPassword) {
        ejs.renderFile(
            './assets/templates/forgotPasswordEmailTemplates.ejs', 
            { 
                firstname: firstname,
                password: newPassword,
            }, 
            function (err, data) {
                if (err) {
                    console.log(err);
                } else {
                    var mail = {}; 
                    mail.subject = "Reset Password";
                    mail.body = data;
                    sendEmail(email, mail);
                }
            });
    },

    forgotEmailMail: function(dob, region, gender, phoneNo) {
        ejs.renderFile(
            './assets/templates/forgotEmailEmailTemplates.ejs', 
            { 
                dob: dob,
                region: region,
                gender: gender,
                phoneNo: phoneNo,
            }, 
            function (err, data) {
                if (err) {
                    console.log(err);
                } else {
                    var mail = {}; 
                    mail.subject = "Participant forgot email";
                    mail.body = data;
                    sendEmail(sails.config.custom.studyEmail, mail);
                }
            });
    },


    sendContactEmail: function(contactEmail) {
      ejs.renderFile(
          './assets/templates/sendContactEmailTemplate.ejs',
          {
            username: contactEmail.username,
            mobile_number: contactEmail.mobile_number,
            contact_reason: contactEmail.contact_reason,
            message: contactEmail.message,
          },
          (err, data) => {
              if (err) {
                  console.log(err);
              }
              else {
                let mail = {};
                mail.subject = "Contact form submission received";
                mail.body = data;
                sendEmail(sails.config.custom.studyEmail, mail);
              }
          }
      );  
    },


}