
module.exports = {
    baseUrl: sails.config.custom.baseUrl,

    /**
     * Get file
     */
    getFile: function (fileDescriptor) {
        var SkipperDisk = require('skipper-disk');
        var fileAdapter = SkipperDisk();

        /// Stream the file down
        return fileAdapter.read(fileDescriptor);
    },

    /**
     * Create file with binary format
     */
    createFile: function(uid, file) {
        return new Promise(function(resolve, reject) {
            file.upload({
                // don't allow the total upload size to exceed ~10MB
                dirname: FileService.getImagePath(),
                maxBytes: sails.config.custom.maxFileSize,
            }, function whenDone(err, uploadedFiles) {

                if (err) {
                    return reject(err);
                }

                // If no files were uploaded, respond with an error.
                if (uploadedFiles.length === 0) {
                    return reject('No file was uploaded');
                }

                // Get the base URL for our deployed application from our custom config
                var uuid = (uploadedFiles[0].fd.split('\\').pop().split('/').pop().split('.'))[0];

                FileService.createFileData(uuid, uploadedFiles[0].fd, uploadedFiles[0].size, uploadedFiles[0].type, uploadedFiles[0].filename, uid)
                    .then(file => {
                        return resolve(file);
                    })
                    .catch(err => {
                        return reject(err);
                    });
            });
        });
    },

    /**
     * Create file with base64 format
     */
    createFileInBase64(uid, originalFilename, dataString) {
        return new Promise(function(resolve, reject) {
            try {
                var matches = dataString.match(/^data:([A-Za-z-+\/]+);base64,(.+)$/),
                decoded = {};
                if (!matches || matches.length !== 3) {
                    throw 'Invalid input string';
                }
                        
                const mime = require('mime-types')
                decoded.type = matches[1];
                decoded.data = Buffer.from(matches[2], 'base64');
                var ext = mime.extension(decoded.type);
                var size = decoded.data.length;
                if (!ext) {
                    throw 'Invalid mime type';
                }

                if (size > sails.config.custom.maxFileSize) {
                    throw 'Exceed the upload limit';
                }

                const uuidv4 = require('uuid/v4');
                var  uuid = uuidv4();
                const fs = require('fs');
                var path = FileService.getImagePath();
                var fullFileName = path + uuid + "." + ext;
                
                if (!fs.existsSync(path)) {
                    fs.mkdirSync(path, { recursive: true });
                }
                fs.writeFileSync(fullFileName, decoded.data, 'base64');
                FileService.createFileData(uuid, fullFileName, size, decoded.type, originalFilename, uid)
                    .then(file => {
                        return resolve(file);
                    })
                    .catch(err => {
                        return reject(err);
                    });

            } catch (err) {
                return reject(err);
            }
        });
    },

    /**
     * Create file metadata
     */
    createFileData(uuid, fd, size, type, filename, uid) {
        return File.create({
            url: require('util').format('%s/api/v1/file/%s', FileService.baseUrl, uuid),
            uuid: uuid,
            fd: fd,
            size: size,
            type: type,
            name: filename,
            createdBy: uid,
            updatedBy: uid,
        }).fetch();
    },

    /**
     * Get image path
     */
    getImagePath() {
        var moment = require("moment");
        var month = moment().format("YYYYMM");
        return sails.config.custom.imagePath + month + "/";
    },

    /**
     * Get file metadata by file id
     */
    getFileDataById(id) {
        return new Promise(function(resolve, reject) {
            File.findOne({ id: id}).exec(
                function(err, result) {
                    if (err) return reject(err);
                    return resolve(result);
                }
            )
        })
    }
}