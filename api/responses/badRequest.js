/**
 * 400 (Bad request) Response
 * Specifically for authentication failed or not yet provided.
 */
module.exports = function (data, code, message, root) {
    var response = _.assign({
      code: code || 'E_BAD_REQUEST',
      message: message || 'Bad requeste',
      data: data || {}
    }, root);
   
    this.req._sails.log.silly('Sent (400 Bad Request)\n', response);
   
    this.res.status(400);
    this.res.json(response);
  };