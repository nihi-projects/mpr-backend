/**
 * 500 (Internal server error) Response
 * Specifically for authentication failed or not yet provided.
 */
module.exports = function (data, code, message, root) {
    var response = _.assign({
      code: code || 'E_INTERNAL_SERVER_ERROR',
      message: message || 'Internal server error',
      data: data || {}
    }, root);
   
    this.req._sails.log.silly('Sent (500 INTERNAL SERVER ERROR)\n', response);
   
    this.res.status(500);
    this.res.json(response);
  };