/**
 * isAuthenticated
 * @description :: Policy to inject user in req via JSON Web Token
 */
var passport = require('passport');
 
module.exports = function (req, res, next) {
  if (!AccessControlService.isAdmin(req.user))  
      return res.unauthorized("No admin right");
  next();
};