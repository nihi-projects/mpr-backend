/**
 * Question.js
 *
 * @description :: A model definition represents a database table/collection.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {

  attributes: {
    questionnaire_id: {
      model: 'questionnaire'
    },
    code: {
      type: 'string'
    },
    answers: {
      collection: 'answer',
      via: 'question_id'
    },
    // remove default attrbutes, only parent questionnaire store these attributes
    status: false,
    createdAt: false,
    createdBy: false,
    updatedAt: false,
    updatedBy: false,
  },

};

