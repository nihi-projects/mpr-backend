/**
 * Study.js
 *
 * @description :: A model definition represents a database table/collection.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {

    attributes: {
      
      consent_date: {
        type: 'number'
      },
      randomize_date: {
        type: 'number'
      },
      group: {
        type: 'string'
      },
      gender: {
        type: 'string'
      },
      dob: {
        type: 'string'
      },
      ethnicity: {
        type: 'json'
      },
      ethnicity_other: {
        type: 'string'
      },
      journey: {
        type: 'string'
      },
      state: {
        type: 'string'
      },
      
      uid: {
        model:'user',
        unique: true
      }
      
    },
    beforeUpdate: function (values, next) {
      next();
    },
    beforeCreate: function (values, next) {
      next();
    }
  
  };
  
  