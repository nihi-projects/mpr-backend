/**
 * Notification.js
 *
 * @description :: A model definition represents a database table/collection.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {

  attributes: {
    
    subject: {
      type: 'string'
    },
    body: {
      type: 'string'
    },
    link: {
      type: 'string'
    },
    read: {
      type: 'boolean'
    }
  },
  beforeUpdate: function (values, next) {
    next();
  },
  beforeCreate: function (values, next) {
    next();
  }

};

