/**
 * Questionnaire.js
 *
 * @description :: A model definition represents a database table/collection.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {

  attributes: {

    type: {
      type: 'string'
    },
    name: {
      type: 'string'
    },
    completed: {
      type: 'boolean'
    },
    finished_at: {
      type: 'number',
      description: 'A JS timestamp (epoch ms) representing the moment at which user revoke their token',
      example: 1502844074211
    },
    total_score: {
      type: 'number'
    },
    summary: {
      type: 'string'
    },
    questions: {
      collection: 'question',
      via: 'questionnaire_id'
    }

  },

};

