/**
 * Answer.js
 *
 * @description :: A model definition represents a database table/collection.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {

  attributes: {
    question_id: {
      model: 'question'
    },
    text: {
      type: 'string'
    },
    score: {
      type: 'number'
    },
    // remove default attrbutes, only parent questionnaire store these attributes
    status: false,
    createdAt: false,
    createdBy: false,
    updatedAt: false,
    updatedBy: false,
  },

};

