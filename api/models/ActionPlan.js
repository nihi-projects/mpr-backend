/**
 * ActionPlan.js
 *
 * @description :: A model definition represents a database table/collection.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {
  
  attributes: {

    when_well_appetite: { type: 'string' },
    when_well_activity: { type: 'string' },
    when_well_sputum: { type: 'string' },
    when_well_sleep: { type: 'string' },
    severe_days: { type: 'string' },
    severe_action: { type: 'string' },
    emergency_action: { type: 'string' },
    daytime_phone: { type: 'string' },
    after_hours_phone: { type: 'string' },

  },
    
};

