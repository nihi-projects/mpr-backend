/**
 * File.js
 *
 * @description :: A model definition represents a database table/collection.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {

  attributes: {

    url: {
      type: 'string'
    },

    uuid:{
      type: 'string'
    },

    fd: {
      type: 'string'
    },

    size: {
      type: 'number'
    },

    type: {
      type: 'string'
    },

    name: {
      type: 'string'
    }
  },
  customToJSON: function () {
    return _.omit(this, ['fd'])
  },
};

