/**
 * Profile.js
 *
 * @description :: A model definition represents a database table/collection.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {

  attributes: {
    
    uid: {
      model: 'user',
      unique: true
    },

    username: {
      type: 'string',
      required: true,      
    },

    mobile_number: {
      type: 'string',
      required: true
    },

    email: {
      type: 'string',
      isEmail: true,            
    },    

    exercise_capacity: {
      type: 'number',
      required: true
    },
    
    /*
    first_name: {
      type: 'string'
    },
    last_name: {
      type: 'string'
    },
    gender: {
      type: 'string'
    },
    dob: {
      type: 'string'
    },
    ethnicity: {
      type: 'json'
    },
    ethnicity_other: {
      type: 'string'
    },
    region: {
      type: 'string'
    },
    sub_region: {
      type: 'string'
    },
    avatar_id: {
      type: 'string'
    },
    consent_date: {
      type: 'number'
    },
    randomisation: {
      type: 'string'
    },
    user_picture: {
      model: 'file'
    },
    journey: {
      type: 'string'
    },
    */
    
  },
  beforeUpdate: function (values, next) {
    next();
  },
  beforeCreate: function (values, next) {
    next();
  }

};

