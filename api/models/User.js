/**
 * User.js
 *
 * @description :: A model definition represents a database table/collection.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

const USER_ROLES = ['admin', 'user'];

module.exports = {
  schema: true,
  attributes: {

    study_id: {
      type: 'number',      
      required: true,
      unique: true 
    },
        
    password: {
      type: 'string',
      required: true
    },
        
    role: {
      type: 'string',
      defaultsTo: 'user'
    },
    device_token: {
      type: 'json',
      description: 'A list of device tokens for sending app notification',
    },
    revoke_time: {
      type: 'number',
      description: 'A JS timestamp (epoch ms) representing the moment at which user revoke their token',
      example: 1502844074211,
      defaultsTo: 0
    },
    state: {
      type: 'string',
      description: 'The latest state of the user in the app'
    },
    
    profile: {
      collection:'profile',
      via: 'uid'      
    },



  },
  customToJSON: function () {
    return _.omit(this, ['password'])
  },
  beforeUpdate: function (values, next) {
    CipherService.hashPassword(values);
    values.updatedBy = values.id;
    next();
  },
  beforeCreate: function (values, next) {
    CipherService.hashPassword(values);
    next();
  },
  roles: USER_ROLES
};

