module.exports = {

    friendlyName: 'Get First Json Element From Array',
  
  
    description: 'Return the first element in json based on the provide array. It used to handle one to one relationship that return array in populate.',

    sync: true,
  
    inputs: {
  
        arr: {
            type: 'ref',
            description: 'The first element in array.',
            required: true
         }
  
    },
  
  
    fn: function(inputs, exits) {
        arr = inputs.arr;
        if (arr === null || arr === undefined) {     // null or undefined, return empty object
            return exits.success(null);
        } else if ( Array.isArray(arr)) {
            if (arr.length > 0) {                   // array with value, get first one
                return exits.success(arr[0]);
            } else {                                // array without value, return empty object
                return exits.success(null);
            }
        } else if (typeof arr === "object") {        // input is object, return object
            return exits.success(arr);
        } else {               
            return exits.success(null);
        }
    }
};