module.exports = {

    friendlyName: 'Generate Random Password',
  
  
    description: 'Generate random password when user need to reset password',

    sync: true,

    inputs: {
  
  
    },
  
    fn: function(inputs, exits) {
        var length = 8,
        charset = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789",
        retVal = "";
        for (var i = 0, n = charset.length; i < length; ++i) {
            retVal += charset.charAt(Math.floor(Math.random() * n));
        }
        return exits.success(retVal);
    }
};